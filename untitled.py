from flask import Flask,render_template

app = Flask(__name__)


@app.route('/login')
def adminlogin():
    return render_template('Admin_login.html')
@app.route('/home')
def adminhome():
    return render_template('Admin_Home.html')
@app.route('/criminal_profile')
def criminalprofile():
    return render_template('Criminal_Profile.html')
@app.route('/user_home')
def userhome():
    return render_template('User_Home.html')
@app.route('/add_caseinfo')
def addcaseinfo():
    return render_template('add_caseinfo.html')
@app.route('/view_caseinfo')
def viewcaseinfo():
    return render_template('View_caseinfo.html')
@app.route('/view_criminalprofile')
def viewcriminalprofile():
    return render_template('View_criminalprofile.html')




if __name__ == '__main__':
    app.run()
